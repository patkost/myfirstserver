package pl.sda;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Hello world!
 */
public class Server {

    public static final int PORT = 9002;
    public static String hostname = "localhost";


    public static void main(String[] args) {


        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            Socket connection = serverSocket.accept();
            InputStream inputStream = connection.getInputStream();
            OutputStream outputStream = connection.getOutputStream();

            Scanner scanner = new Scanner(inputStream, "UTF-8");

            PrintWriter pw = new PrintWriter(new OutputStreamWriter(outputStream, "UTF-8"), true);


            pw.println("Hello, Please enter your name! :)");

            while (scanner.hasNext()) {
                String message = scanner.nextLine();
                System.out.println("Received message for client: " + message);


                if (message.equalsIgnoreCase("stop")) {
                    System.out.println("Received stop command!");
                    break;
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();

        }
    }
}
